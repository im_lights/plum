MAIN = main.cpp
CC = g++
COMPILER_FLAGS = -std=gnu++14 -w
LINKER_FLAGS = src/util.cpp src/feed.cpp src/camera.cpp src/display.cpp src/terminal.cpp src/core.cpp -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer
OBJ_NAME = Plum
DEBUG_OUT = build/debug/
RELEASE_OUT = build/release/

all : $(OBJS)
	$(CC) -I/home/sysadmin/Repositories/plum/ -I/home/sysadmin/Repositories/plum/include/ -I/home/sysadmin/Repositories/plum/lib $(MAIN) $(LINKER_FLAGS) $(COMPILER_FLAGS) -o $(RELEASE_OUT)$(OBJ_NAME)
	
debug : $(OBJS)
	$(CC) -fpermissive -I/home/sysadmin/Repositories/plum/ -I/home/sysadmin/Repositories/plum/include/ -I/home/sysadmin/Repositories/plum/lib $(MAIN) $(LINKER_FLAGS) $(COMPILER_FLAGS) -o $(DEBUG_OUT)$(OBJ_NAME)
	
camera : $(OBJS)
	$(CC) -I/home/sysadmin/Repositories/plum/include/ -I/home/sysadmin/Repositories/plum/lib src/camera.cpp $(COMPILER_FLAGS) -o $(DEBUG_OUT)camera.o
	
terminal : $(OBJS)
	$(CC) -I/home/sysadmin/Repositories/plum/include/ src/terminal.cpp $(COMPILER_FLAGS) -o $(DEBUG_OUT)terminal.o
	
feed : $(OBJS)
	$(CC) -I/home/sysadmin/Repositories/plum/include/ src/feed.cpp $(COMPILER_FLAGS) -o $(DEBUG_OUT)feed.o