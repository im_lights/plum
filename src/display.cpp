#include <display.h>

Display::Display(std::string name, FeedPipe* _pipe)
{
	id = name;
	pipe = _pipe;
	
	// Init
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		std::cerr << "Could not Initialize SDL" << std::endl;
		window = SDL_CreateWindow(
			name.c_str(), // Change me
			SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED,
			pipe->width,
			pipe->height,
			SDL_WINDOW_SHOWN
		);
		if (window == NULL)
			std::cerr << "Window could not be created" << std::endl;
		else
			screen = SDL_GetWindowSurface(window);
			
		renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
		SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
		
	}
}

void Display::FormatData()
{
	bitmap_image frame(pipe->width, pipe->height);
	frame.clear();
	// TODO - Iterate through v4l2 data buffer and create bmp image from pixel data
}

DisplayManager::DisplayManager()
{
	// We don't really need to use this for anything
}

bool DisplayManager::Init(std::vector<FeedPipe*> pipes)
{
	for (int i = 0; i < pipes.size(); i++)
	{
		displays.insert(std::pair<std::string, Display*>(pipes[i]->id, new Display(pipes[i]->id, pipes[i])));
	}
}

bool DisplayManager::Tick(std::chrono::milliseconds now)
{
	// Write Me
	return true;
}