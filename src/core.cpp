#include <core.h>

template<class Fn, class... Args>
static std::thread SPAWN_THREAD(Fn &&function, Args&&... args)
{
	return std::thread(function, args...);
}

enum Status {
	waiting,
	action,
	error,
	paused
};

bool Core::Init()
{
	if (!LoadTerminal())
	{
		return false;
	}
	
	if (!LoadSettings())
	{
		return false;
	}
	
	if (!LoadDevices())
	{
		return false;
	}
	
	if (!LoadUI())
	{
		return false;
	}
}

bool Core::LoadSettings()
{
	std::ifstream stream("settings.cfg");
	std::string data;
	std::string buffer;
	
	while(getline(stream, buffer))
	{
		data += buffer;
	}
	
	if (data.size() == 0)
	{
		std::cout << "StartupError: Unable to load settings.cfg" << std::endl;
		return false;
	}
	else
	{
		const char n = '/n';
		std::vector<std::string> lines = Util::split(data, &n);
		for (unsigned int i = 0; i < lines.size(); i++)
		{
			switch(i)
			{
			case 0: // Devmode
			{
				devmode = lines[i].at(0) == '1' ? true : false;
			}
			break;
			case 1: // Terminal Mode
			{
				term->SetMode(lines[i].at(0) = '1' ? true : false);
			}
			break;
			}
		}
		std::cout << "Settings Loaded Successfully." << std::endl;
		return true;
	}
}

bool Core::LoadTerminal()
{
	std::map<std::string, Command<Core>> cmds;
	
	if (!Terminal<Core>::LoadCommands(false, &cmds, this)) return false;
	
	term = new Terminal<Core>(cmds, this);
	
	return true;
}

bool Core::LoadDevices()
{
	std::ifstream stream("camdata.cfg");
	std::string data;
	std::string buffer;
	
	while(getline(stream, buffer))
	{
		data += buffer;
	}
	
	std::vector<CamData> camdata;
	
	if (data.size() == 0)
	{
		std::cout << "StartupError: Unable to load camdata.cfg" << std::endl;
		return false;
	}
	else
	{
		const char n = '/n';
		const char m = ',';
		std::vector<std::string> lines = Util::split(data, &n);
		for (unsigned int i = 0; i < lines.size(); i++)
		{
			std::vector<std::string> parts = Util::split(lines[i], &m);
			camdata.push_back(CamData(parts[0], parts[1], parts[2] == "1" ? true : false, parts[3] == "1" ? true : false, stoi(parts[4]), stoi(parts[5])));
		}
		std::cout << "CamData Loaded Successfully." << std::endl;
		return true;
	}
	
	devMgr = new DeviceManager();
	return devMgr->Init(camdata);
}

bool Core::LoadUI()
{
	dispMgr = new DisplayManager();
	return dispMgr->Init(devMgr->AttachPipes());
}

void Core::RefreshDevices()
{
	std::chrono::milliseconds now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch());;
	std::chrono::milliseconds ratems(Rate);
	const std::chrono::milliseconds TIME_LEFT(0);
act:
	if (!devMgr->Tick(now) || !dispMgr->Tick(now))
		goto err;
	else
		this->LastTick = now;
wait:
	now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch());
	if (LastTick + ratems - now > TIME_LEFT)
		goto wait;
	else
		goto act;
err:
	if (CanRun())
		goto wait;
	else
		return;
}

int Core::Main()
{
	if (!LoadSettings())
		return 1;
		
	if (!LoadTerminal())
		return 2;
		
	if (!LoadDevices())
		return 3;
		
	if (!LoadUI())
		return 4;
		
	th0 = SPAWN_THREAD(&Terminal<Core>::Listen, term);
	//th0 = this->thread(&Core::RefreshDevices(), this);
	//th1 = this->thread(&Core::LaunchGtk, this);
	while(CanRun())
	{
		
	}
	th0.join();
	//th1.join();
	return 1;
}

bool Core::CanRun()
{
	return true;
}
