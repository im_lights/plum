#include <camera.h>

static int xioctl(int fh, int request, void *arg)
{
	int r;
	do {
		r = ioctl(fh, request, arg);
	} while (-1 == r && EINTR == errno);
	return r;
}

Camera::Camera(CamData data) : cam_id(data.id), d(data)
{
	const char* id_c = data.id.c_str();
	struct v4l2_capability cap;
	struct v4l2_cropcap cropcap;
	struct v4l2_crop crop;
	struct v4l2_format fmt;
	unsigned int min;

	int camera_fd = open(id_c, O_RDONLY);

	if (-1 == xioctl(camera_fd, VIDIOC_QUERYCAP, &cap))
	{
		if (EINVAL == errno)
		{
			fprintf(stderr, "%s is no V4L2 device\n", id_c);
			exit(EXIT_FAILURE);
		}
		else
		{
			printf("\nError in ioctl VIDIOC_QUERYCAP\n\n");
			exit(0);
		}
	}

	if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE))
	{
		fprintf(stderr, "%s is no video capture device\n", id_c);
		exit(EXIT_FAILURE);
	}

	if (!(cap.capabilities & V4L2_CAP_STREAMING))
	{
		fprintf(stderr, "%s does not support streaming i/o\n", id_c);
		exit(EXIT_FAILURE);
	}
	
	// std::CLEAR(cropcap);
	cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	
	if (0 == xioctl(fd, VIDIOC_CROPCAP, &cropcap))
	{
		crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		crop.c = cropcap.defrect; /* reset to default */
		
		if (-1 == xioctl(fd, VIDIOC_S_CROP, &crop))
		{
			switch (errno)
			{
			case EINVAL:
				/* Cropping not supported. */
				break;
			default:
				/* Errors ignored. */
				break;
			}
		}
	}
	else
	{
		// Errors ignored.
	}
	
	// CLEAR(fmt);
	// memset(&fmt, 0, sizeof(fmt));
	fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	fmt.fmt.pix.width = d.width;
	fmt.fmt.pix.height = d.height;
	fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_RGB24;
	fmt.fmt.pix.field = V4L2_FIELD_INTERLACED;
	if (-1 == xioctl(camera_fd, VIDIOC_S_FMT, &fmt))
	{
		printf("VIDIOC_S_FMT");
	}
	
	min = fmt.fmt.pix.width * 2;
	if (fmt.fmt.pix.bytesperline < min)
		fmt.fmt.pix.bytesperline = min;
	min = fmt.fmt.pix.bytesperline * fmt.fmt.pix.height;
	if (fmt.fmt.pix.sizeimage < min)
		fmt.fmt.pix.sizeimage = min;
	
	bsize = fmt.fmt.pix.sizeimage;
	fd = camera_fd;
	state = inactive;
}

bool Camera::SetState(CamState c)
{
	if (state != dead)
	{
		state = c;
		return true;
	}
	else
		return false;
}

CamState Camera::GetState()
{
	return state;
}

bool Camera::isDead()
{
	if (state == dead)
		return true;
	else
		return false;
}

std::string Camera::Id()
{
	return cam_id;
}

int Camera::GetHeight()
{
	return d.height;
}

int Camera::GetWidth()
{
	return d.width;
}

CameraController::CameraController(Camera* c)
{
	std::memset(&reqbuf, 0, sizeof(reqbuf));
	std::memset(&audio, 0, sizeof(audio));
	cam = c;
}

bool CameraController::Init()
{
	reqbuf.count  = 4;
	reqbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	reqbuf.memory = V4L2_MEMORY_USERPTR;

	if (ioctl(cam->fd, VIDIOC_REQBUFS, &reqbuf) == -1)
	{
		std::cout << "REQBUFS error" << std::endl;
		return false;
	}

	m_buf = (buffer*)calloc(4, sizeof(*m_buf));

	if (!m_buf) {
		std::cout << "Out of Memory" << std::endl;
		return false;
	}

	for (count = 0; count < 4; ++count) {
		m_buf[count].length = cam->BufferSize();
		m_buf[count].start = malloc(cam->BufferSize());

		if (!m_buf[count].start) {
			std::cout << "Out of Memory" << std::endl;
			return false;
		}
	}
	
	return true;
}

void CameraController::StartStream()
{
	// std::CLEAR(buf);
	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf.memory = V4L2_MEMORY_USERPTR;
	buf.index = 0;
	buf.m.userptr = (unsigned long)m_buf->start;
	buf.length = m_buf->length;
	
	if (-1 == xioctl(cam->fd, VIDIOC_QBUF, &buf))
	{
		std::cout << "VIDIOC_QBUF Error" << std::endl;
		return;
	}
		
	type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if (-1 == xioctl(cam->fd, VIDIOC_STREAMON, &type))
	{
		std::cout << "VIDIOC_STREAMON Error" << std::endl;
		return;
	}
	std::cout << "StartStream Success" << std::endl;
}

void CameraController::KillStream()
{
	if (ioctl(cam->fd, VIDIOC_STREAMOFF, &reqbuf) == -1)
		printf("Unable to kill Camera Stream\\n");
	else
		perror("VIDIOC_STREAMOFF");
}

void CameraController::StartRecord()
{
	//if (ioctl(cam->fd, ))
}

void CameraController::KillRecord()
{
	
}

CameraController* SpawnCam(CamData data)
{
	Camera* cam = new Camera(data);
	if (cam->isDead())
	{
		printf("Unable to spawn Cam");
		return nullptr;
	}
	
	CameraController* r_val = new CameraController(cam);
	if (r_val->Init())
		return r_val;
	else
		return nullptr;
}

void CameraController::Tick(std::chrono::milliseconds now)
{
looplogic:
	fd_set fds;
	struct timeval tv;
	int r;

	FD_ZERO(&fds);
	FD_SET(cam->fd, &fds);

	/* Timeout. */
	tv.tv_sec = 2;
	tv.tv_usec = 0;

	r = select(cam->fd + 1, &fds, NULL, NULL, &tv);

	if (-1 == r) {
		std::cout << "Tickloop fail" << std::endl;
	}

	if (0 == r) {
		fprintf(stderr, "select timeout\\n");
		return;
	}

readframe:
	// Read Frame
	v4l2_buffer buf;
	// std::CLEAR(buf); Requires another include
	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf.memory = V4L2_MEMORY_USERPTR;

	if (-1 == xioctl(cam->fd, VIDIOC_DQBUF, &buf))
	{
		std::cout << "DQBUF ERR" << std::endl;
	}

/*
	if (buf.m.userptr == (unsigned long)m_buf.start && buf.length == m_buf.length)
		break;
*/
	// We may need to replace this later with a data pipe
	ProcessImage((void *)buf.m.userptr, buf.bytesused, this->cam);

	if (-1 == xioctl(cam->fd, VIDIOC_QBUF, &buf))
		std::cout << "qbuf err" << std::endl;
}

FeedPipe* CameraController::SpawnPipe()
{
	pipe = new FeedPipe(cam->Id(), cam->GetHeight(), cam->GetWidth());
	return pipe;
}

void CameraController::ProcessImage(const void *p, int size, Camera* cam)
{
	pipe->write(p, size);
}

Camera* CameraController::GetCam()
{
	return cam;
}

DeviceManager::DeviceManager()
{
	
}

DeviceManager::~DeviceManager()
{
	std::map<std::string, CameraController*>::iterator it;
	for (it = devices.begin(); it != devices.end(); it++)
	{
		delete it->second;
		devices.erase(it);
	}
}

bool DeviceManager::Init(std::vector<CamData> data)
{
	// Add detected devices
	for (unsigned int i = 0; i < data.size(); i++)
	{
		CameraController* c = SpawnCam(data[i]);
		if (c == nullptr)
		{
			// Error
		}
		else
		{
			devices.insert(std::pair<std::string, CameraController*>(c->GetCam()->Id(), c));
		}
	}
}

bool DeviceManager::Tick(std::chrono::milliseconds now)
{
	std::map<std::string, CameraController*>::iterator it;
	for (it = devices.begin(); it != devices.end(); it++)
	{
		it->second->Tick(now);
	}
}

std::vector<FeedPipe*> DeviceManager::AttachPipes()
{
	std::vector<FeedPipe*> pipes;
	std::map<std::string, CameraController*>::iterator it;
	for (it = devices.begin(); it != devices.end(); it++)
	{
		pipes.push_back(it->second->SpawnPipe());
	}
	return pipes;
}