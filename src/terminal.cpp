#include <terminal.h>

static const std::string PROMPT_A = "[Advanced Mode] >:";
static const std::string PROMPT_B = "[Basic Mode] >:";
static const char INPUT_D = ',';

static int levenshtein(std::string s, std::string t, int l)
{
	int n = s.length();
	int m = t.length();
	
	if (n == 0)
		return m;
	if (m == 0)
		return n;
	if (l && std::abs(m - n) > 1)
		return std::abs(m - n);
	
	int d[n + 1][m + 1];
	for (int i = n; i >= 0; i--)
		d[i][0] = i;
	for (int j = m; j >= 0; j--)
		d[0][j] = j;
	
	char* s_i = nullptr;
	char* t_j = nullptr;
	int cost;
	int b;
	int c;
	int min;
	for (int i = 1; i <= n; i++)
	{
		s_i = &s[i - 1];
		for (int j = 1; j <= m; j++)
		{
			if (i == j &&  d[i][j] > 4)
				return n;
			t_j = &t[j - 1];
			cost = *s_i != *t_j ? 1 : 0;
			min = d[i - 1][j] + 1;
			b = d[i][j - 1] + 1;
			c = d[i - 1][j - 1] + cost;
			
			if (b < min)
				min = b;
			if (c < min)
				min = c;
			d[i][j] = min;
		}
	}
	return d[n][m];
}

// Argument Struct Definitions

Argument::Argument(const std::string _key, const ArgType _type) : key(_key), type(_type)
{
	
}

// Command Struct Definitions
template<class Core>
Command<Core>::Command(const std::string _key, const std::vector<Argument> _args, const std::vector<std::string> _req, void* lamda) : key(_key), args(_args), requiredArgs(_req), func(lamda)
{
	
}

template<class Core>
bool Command<Core>::ParseArgument(std::string in, std::deque<ParsedArgument> &args_deq)
{
	bool def;
	std::string key;
	
	std::string in0;
	if (in[0] == '-' && in[1] == '-')
	{
		def = true;
		in0 = in.substr(2);
	}
	else if (in[0] == '-')
	{
		def = false;
		in0 = in.substr(1);
	}
	else
	{
		//Throw Syntax Error
		std::cout << "(rewrite me) Syntax Error parsing argument" << std::endl;
		return false;
	}
	std::pair<std::string, std::string> parts = Util::pairify(in0);
	int index = -1;
	for (int i = 0; i < args.size(); i++)
	{
		if (args[i].key == Util::toId(parts.first()))
		{
			index = i;
			key = args[i].key;
			break;
		}
	}
	if (index < 0)
		return false; // Couldn't find the arg
	
	ParsedArgument p(def, key, args[index].type, parts.second());
	args_deq.push_back(p);
	return true;
}

template<class Core>
bool Command<Core>::run(std::deque<ParsedArgument> args)
{
	bool(*callback)(std::deque<ParsedArgument>) = func;
	return callback(args);
}

// Terminal Class Definitions

template<class Core>
Terminal<Core>::Terminal(const std::map<std::string, Command<Core>> _cmds, Core* _m) : commands(_cmds), Master(_m)
{
	
}

template<class Core>
bool Terminal<Core>::LoadCommands(bool full, std::map<std::string, Command<Core>>* cmds, Core* m)
{
	// std::vector<Argument> 
	Command<Core> cmd_advmode("advmode", std::vector<Argument> { Argument("value", ArgType::argBool) }, std::vector<std::string> { "value" }, [core = m](std::deque<ParsedArgument>)
	{
		/*
		if (args[0].type == ArgType::argBool && args[0].key == "value")
		{
			bool val = args[0].value == "true" ? true : false;
			core->term->cmdMode = val;
			if (val)
				core->term->write("Advanced Mode enabled.");
			else
				core->term->write("Advanced Mode disabled.");
			return true;
		}
		else
		{
			// Error - invalid argument
			return true;
		}
		 */
	});
	cmds->insert(std::pair<std::string, Command<Core>>("advmode", cmd_advmode));
	
	Command<Core> cmd_exit("exit", std::vector<Argument>({}), std::vector<std::string>({}), [core = m](std::deque<ParsedArgument> args){
		core->term->write("Exiting...");
		core->term->exit();
		return true;
	});
	cmds->insert(std::pair<std::string, Command<Core>>("exit", cmd_exit));
	
	Command<Core> cmd_help("help", std::vector<Argument>({ Argument("id", ArgType::argString), Argument("verbose", ArgType::argBool) }), std::vector<std::string>({}), [core = m](std::deque<ParsedArgument> args){
		// TODO - Serve help command
	});
	cmds->insert(std::pair<std::string, Command<Core>>("help", cmd_help));
	
	Command<Core> cmd_opencam("opencam", std::vector<Argument>({ Argument("id", ArgType::argString) }), std::vector<std::string>({ "id" }), [core = m](std::deque<ParsedArgument> args){
		// TODO - Open Camera
		/*
		if (!core->devMgr->CheckDevice(args[0].value))
		{
			core->term->write("Unable to Open Camera: Camera '" + args[0].value + "' does not exist.");
			return true;
		}
		CameraController* c = core->devMgr->devices[args[0].value];
		if (c->cam->GetState() == streaming || c->cam->GetState() == all)
		{
			core->term->write("Unable to Open Camera: Camera '" + args[0].value + "' is already streaming.");
			return true;
		}
		c->StartStream();
		c->StartRecord();
		 */
	});
	cmds->insert(std::pair<std::string, Command<Core>>("opencam", cmd_opencam));
	
	Command<Core> cmd_closecam("closecam", std::vector<Argument>({ Argument("id", ArgType::argString) }), std::vector<std::string>({ "id" }), [core = m](std::deque<ParsedArgument> args){
		// TODO - Close Camera
	});
	cmds->insert(std::pair<std::string, Command<Core>>("closecam", cmd_closecam));
	
	Command<Core> cmd_listcams("listcams", std::vector<Argument>({}), std::vector<std::string>({}), [core = m](std::deque<ParsedArgument> args){
		// TODO - List Cameras
	});
	cmds->insert(std::pair<std::string, Command<Core>>("listcams", cmd_listcams));
	
	Command<Core> cmd_recordcam("recordcam", std::vector<Argument>({ Argument("id", ArgType::argString) }), std::vector<std::string>({ "id" }), [core = m](std::deque<ParsedArgument> args){
		// TODO - Record selected Camera
	});
	cmds->insert(std::pair<std::string, Command<Core>>("recordcam", cmd_recordcam));
	
	Command<Core> cmd_stoprecordcam("stoprecordcam", std::vector<Argument>({ Argument("id", ArgType::argString) }), std::vector<std::string>({ "id" }), [core = m](std::deque<ParsedArgument> args){
		// TODO - Stop recording selected Camera
	});
	cmds->insert(std::pair<std::string, Command<Core>>("stoprecordcam", cmd_stoprecordcam));
	
	Command<Core> cmd_stopall("stopall", std::vector<Argument>({}), std::vector<std::string>({}), [core = m](std::deque<ParsedArgument> args){
		// TODO - Stop all recording Cameras
	});
	cmds->insert(std::pair<std::string, Command<Core>>("stopall", cmd_stopall));
	
	Command<Core> cmd_volume("volume", std::vector<Argument>({ Argument("value", ArgType::argNumber) }), std::vector<std::string>({ "value" }), [core = m](std::deque<ParsedArgument> args){
		// TODO - Set Playback volume
	});
	cmds->insert(std::pair<std::string, Command<Core>>("volume", cmd_volume));
	
	Command<Core> cmd_forcemute("forcemute", std::vector<Argument>({}), std::vector<std::string>({}), [core = m](std::deque<ParsedArgument> args){
		// TODO - Manually mute all playback
	});
	cmds->insert(std::pair<std::string, Command<Core>>("forcemute", cmd_forcemute));
	
	if (!full) return true;
	
	// Initialize Advanced Commands
	
	Command<Core> cmd_selectcam("selectcam", std::vector<Argument>({ Argument("id", ArgType::argString) }), std::vector<std::string>({ "id" }), [core = m](std::deque<ParsedArgument> args){
		// TODO - Select Camera
	});
	cmds->insert(std::pair<std::string, Command<Core>>("selectcam", cmd_selectcam));
	
	Command<Core> cmd_stream("stream", std::vector<Argument>({}), std::vector<std::string>({}), [core = m](std::deque<ParsedArgument> args){
		// TODO - Stream Selected Camera
	});
	cmds->insert(std::pair<std::string, Command<Core>>("stream", cmd_stream));
	
	Command<Core> cmd_close("close", std::vector<Argument>({}), std::vector<std::string>({}), [core = m](std::deque<ParsedArgument> args){
		// TODO - Close Selected Camera
	});
	cmds->insert(std::pair<std::string, Command<Core>>("close", cmd_close));
	
	Command<Core> cmd_edit("edit", std::vector<Argument>({}), std::vector<std::string>({}), [core = m](std::deque<ParsedArgument> args){
		// TODO - Edit Selected Camera
	});
	cmds->insert(std::pair<std::string, Command<Core>>("edit", cmd_edit));
	
	Command<Core> cmd_record("record", std::vector<Argument>({}), std::vector<std::string>({}), [core = m](std::deque<ParsedArgument> args){
		// TODO - Record Selected Camera
	});
	cmds->insert(std::pair<std::string, Command<Core>>("record", cmd_record));
	
	Command<Core> cmd_stop("stop", std::vector<Argument>({}), std::vector<std::string>({}), [core = m](std::deque<ParsedArgument> args){
		// TODO - Stop Recording Selected Camera
	});
	cmds->insert(std::pair<std::string, Command<Core>>("stop", cmd_stop));
	
	Command<Core> cmd_camstats("camstats", std::vector<Argument>({ Argument("id", ArgType::argString) }), std::vector<std::string>({ "id" }), [core = m](std::deque<ParsedArgument> args){
		// TODO - Serve Camera recording stats
	});
	cmds->insert(std::pair<std::string, Command<Core>>("camstats", cmd_camstats));
	
	Command<Core> cmd_viewlogs("viewlogs", std::vector<Argument>({}), std::vector<std::string>({}), [core = m](std::deque<ParsedArgument> args){
		// TODO - Serve Log overview
	});
	cmds->insert(std::pair<std::string, Command<Core>>("viewlogs", cmd_viewlogs));
	
	return true;
}

template<class Core>
void Terminal<Core>::AcceptInput()
{
	allowInput = true;
}

template<class Core>
void Terminal<Core>::PauseInput()
{
	allowInput = false;
}

template<class Core>
bool Terminal<Core>::ParseInput(std::string in)
{
	bool failed = false;
	std::string::size_type loc = in.find(PROMPT_A);
	goto locate;
retrylocate:
	failed = true;
locate:
	if (loc == std::string::npos)
		if (!failed)
		{
			loc = in.find(PROMPT_B);
			goto retrylocate;
		}
		else
		{
			goto sanitize; // Lets just hope this isn't an error
		}
	else
	{
		if (failed)
			failed = false; // Reset this variable if it was set to true
		goto sanitize;
	}
	return false; // This should never be called but acts as a break point
sanitize:
	std::string line;
	if (failed)
		line = in; // Somehow we couldn't find the input prompt data in the string
	else
		line = in.substr(loc); // Sanitize Prompter from input
	std::deque<std::string> parts = Util::dsplit(line, &INPUT_D);
	if (parts.size() <= 0)
	{
		// Error - Invalid Input
		return false; // This is a real problem, we need to hard-exit
	}
	std::string command; // Initialize this variable to be set later
	std::deque<ParsedArgument> args;
parse:
	if (parts.size() == 0)
		command = parts.pop_front();
	else
		goto exit; // No further commands to parse
		
	if (!ValidateCommand(&command))
	{
		// Error - Invalid Command
		goto invalid;
	}
	else if (!VerifyArguments(command, &parts, &args))
	{
		// Error - Invalid Arguments
		// VerifyArguments will serve this error
		goto exit;
	}
	else
	{
		goto execute;
	}
execute:
	if (!ExecuteCommand(command, args))
	{
		// Error - Broken command
		return false; // Our shit is fucked
	}
	else if (parts[0] == "&")
	{
		parts[0] = parts[0].substr(1);
		command = "";
		args.clear();
		goto parse; // We have another command to parse
	}
	else
		goto exit;
exit:
	return true;
invalid:
	// TODO: Handle invalid command
	goto exit;
}

template<class Core>
void Terminal<Core>::SetMode(bool m)
{
	cmdMode = m;
	if (cmdMode)
	{
		std::cout << "Notice: Application is running in advanced mode." << std::endl;
	}
	else
	{
		std::cout << "Notice: Application is running in lightweight mode." << std::endl;
	}
}

template<class Core>
void Terminal<Core>::a()
{
	if (!awaiting)
	{
		return;
	}
	else
	{
		awaiting = false;
		if (cmdMode)
		{
			std::cout << PROMPT_A;
		}
		else
		{
			std::cout << PROMPT_B;
		}
	}
}

template<class Core>
void Terminal<Core>::Listen()
{
	std::string line;
	while(true)
	{
		a(); // Display "awaiting input prompt" if not displayed since last input
		std::cin >> line; // Thread will pause here and wait for input
		
		// Input has now been received, parse it
		awaiting = true; // We're ready to display the input prompt on next loop
		if (!ParseInput(&line)) // Pass line to parse input function
			break; // End loop if parse input returns false
		else if (lineHistory.back() != line) // Check to see if current line matches last line
		{
			lineHistory.push_back(line); // Add line to history if unique
			if (lineHistory.size() > 100) // Check to see if we have over 100 lines in history
				lineHistory.pop_front(); // Remove first line in history if over 100
		}
	}
}

template<class Core>
const std::vector<std::string> Terminal<Core>::CommandList()
{
	std::vector<std::string> o;
	for (auto const& x : commands)
	{
		o.push_back(x.first());
	}
	const std::vector<std::string> output = o;
	return output;
}

template<class Core>
bool Terminal<Core>::TryCorrect(std::string input, std::string &output, const std::vector<std::string> commandList)
{
	
}

template<class Core>
bool Terminal<Core>::ValidateCommand(std::string &cmd)
{
	std::string vCmd;
	int index = -1;
	const std::vector<std::string> commandList = CommandList();
	for (int i = 0; i < commandList.size(); i++)
	{
		if (commandList[i] == Util::toId(cmd))
		{
			index = i;
			break;
		}
	}
	if (index < 0)
	{
		if (!TryCorrect(cmd, &vCmd, commandList))
			return false;
		else
		{
			cmd = vCmd;
			return true;
		}
	}
	else
		return true;
}

template<class Core>
bool Terminal<Core>::VerifyArguments(std::string cmd, std::deque<std::string> &parts, std::deque<ParsedArgument> &args)
{
	Command<Core> command = commands[cmd];
	while(true)
	{
		if (parts[0] == "&" || args.size() == command.args.size())
		{
			break;
		}
		if (command.ParseArgument(parts[0], &args))
		{
			parts.pop_front();
		}
	}
	
	if (command.requiredArgs.size() != 0)
	{
		// Ensure we have all required arguments
		int successCount = 0;
		for (unsigned int i = 0; i < command.requiredArgs.size(); i++)
		{
			for (unsigned int j = 0; j < args.size(); j++)
			{
				if (args[j].key == command.requiredArgs[i])
				{
					successCount++;
					break;
				}
			}
		}
		if (successCount == command.requiredArgs.size())
			return true;
		else
			return false;
	}
	return true;
}

template<class Core>
bool Terminal<Core>::ExecuteCommand(std::string cmd, std::deque<ParsedArgument> args)
{
	commands[cmd].run(args, Master);
}