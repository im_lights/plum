#include <util.h>

inline std::vector<std::string> Util::split(std::string s, const char *delim = DEFAULT_SPLIT_DELIM)
{
	std::istringstream stream(s);
	std::vector<std::string> strings;
	std::string each;
	while(std::getline(stream, each, *delim))
	{
		strings.push_back(each);
	}
	return strings;
}

inline std::deque<std::string> Util::dsplit(std::string s, const char *delim = DEFAULT_SPLIT_DELIM)
{
	std::istringstream stream(s);
	std::deque<std::string> strings;
	std::string each;
	while(std::getline(stream, each, *delim))
	{
		strings.push_back(each);
	}
	return strings;
}

inline std::string Util::toId(std::string in)
{
	std::locale loc;
	std::string str;
	for (char c : in)
	{
		if (std::isdigit(c))
			str += c;
		else if (std::isalpha(c))
			str += std::tolower(c, loc);
		else
			continue; //Not alphanumeric
	}
	return str;
}

inline std::pair<std::string,std::string> Util::pairify(std::string input, const char *delim = DEFAULT_PAIR_DELIM)
{
	std::vector<std::string> p = split(input,delim);
	return std::pair<std::string,std::string>(p[0],p[1]);
}