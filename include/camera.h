#ifndef CAMERA_H
#define CAMERA_H

#include <cstring>
#include <util.h>
#include <feed.h>
#include <unistd.h>
#include <iostream>
#include <string>
#include <chrono>
#include <map>
#include <vector>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <linux/videodev2.h>

enum CamState { dead, inactive, recording, streaming, all };
enum v4l2_buf_type type;
struct CamData
{
	CamData(
		std::string _id,
		std::string _name,
		bool _record,
		bool _stream,
		int h,
		int w
	) : id(_id), name(_name), record(_record), stream(_stream), height(h), width(w) {}
	std::string id;
	std::string name;
	bool record;
	bool stream;
	int height;
	int width;
};

struct buffer
{
	void* start;
	size_t length;
};

class Camera
{
public:
	Camera(CamData data);
	~Camera();
	
	bool SetState(CamState c);
	CamState GetState();
	bool isDead();
	std::string Id();
	int GetHeight();
	int GetWidth();
	unsigned int BufferSize();
	
	int fd = 0;
private:
	CamData d;
	std::string cam_id;
	CamState state = dead;
	int out_buf;
	unsigned int bsize;
	// TODO: Add Camera data to this class once the API is figured out
};

class CameraController
{
public:
	CameraController(Camera* c);
	~CameraController();
	
	bool Init();
	void StartStream();
	void KillStream();
	void StartRecord();
	void KillRecord();
	void VolUp();
	void VolDown();
	void VolMute();
	void Disable();
	void Tick(std::chrono::milliseconds now);
	FeedPipe* SpawnPipe();
	Camera* GetCam();
private:
	Camera* cam = nullptr;
	FeedPipe* pipe = nullptr;
	struct v4l2_requestbuffers reqbuf;
	struct v4l2_audio audio;
	struct v4l2_buffer buf;
	struct buffer* m_buf;
	unsigned int count;
	
	void ProcessImage(const void* p, int size, Camera* cam);
};

class DeviceManager
{
public:
	DeviceManager();
	~DeviceManager();
	
	bool Init(std::vector<CamData> data);
	bool Tick(std::chrono::milliseconds now);
	std::vector<FeedPipe*> AttachPipes();
private:
	std::map<std::string, CameraController*> devices;
};

#endif