#ifndef UTIL_H
#define UTIL_H

#include <vector>
#include <string>
#include <deque>
#include <map>
#include <sstream>
#include <iostream>
#include <locale>

static const char D_DELIM = ',';
static const char *DEFAULT_SPLIT_DELIM = &D_DELIM;
static const char P_DELIM = ':';
static const char *DEFAULT_PAIR_DELIM = &P_DELIM;

namespace Util
{
	inline std::vector<std::string> split(std::string s, const char *delim = DEFAULT_SPLIT_DELIM);
	inline std::deque<std::string> dsplit(std::string s, const char *delim = DEFAULT_SPLIT_DELIM);
	inline std::string toId(std::string in);
	inline std::pair<std::string,std::string> pairify(std::string input, const char *delim = DEFAULT_PAIR_DELIM);
/*
	std::map<std::string,std::string> mapify(std::vector<std::string> input) {
		std::map<std::string,std::string> out;
		for (unsigned int i = 0; i < input.size(); i++) {
			out.insert(std::pair<std::string,std::string>(pairify(input[i])));
		}
		return out;
	}
	 */
};

#endif