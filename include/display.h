#ifndef DISPLAY_H
#define DISPLAY_H

#include <util.h>
#include <chrono>
#include <feed.h>
#include <bitmap_image.hpp>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>
#include <string>
#include <vector>
#include <map>

typedef SDL_Renderer REN;
typedef SDL_Window WIN;
typedef SDL_Texture TEX;
typedef SDL_Surface SFC;

class Display
{
public:
	Display(std::string name, FeedPipe* _pipe);
	~Display();
private:
	std::string id;
	REN* renderer = NULL;
	WIN* window = NULL;
	SFC* screen = NULL;
	FeedPipe* pipe;
	
	void FormatData();
};

class DisplayManager
{
public:
	DisplayManager();
	~DisplayManager();
	
	bool Init(std::vector<FeedPipe*> pipes);
	bool SpawnDisplay();
	Display* GetDisplay(int index);
	bool CloseDisplay(int index);
	bool Tick(std::chrono::milliseconds now);
private:
	std::map<std::string, Display*> displays;
};

#endif