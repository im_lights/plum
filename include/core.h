#ifndef CORE_H
#define CORE_H

// Core Includes
#include "feed.h"
#include "camera.h"
#include "display.h"
#include "terminal.h"

#include <thread>
#include <chrono>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

std::chrono::milliseconds zero = std::chrono::milliseconds(0);

class Core
{
public:
	Core();
	virtual ~Core();
	
	bool Init();
	int Main();
	//Glib::RefPtr<Gtk::Application> SpawnGtkApp();
	
	//Glib::RefPtr<Gtk::Application> app = // Gtk::Application::create(argc, argv, "org.gtkmm.example");
	
	// Settings
	bool devmode;
	
	static const int Rate = 60; // Device Refresh Rate
	std::chrono::milliseconds LastTick = zero;
	// = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch());
private:
	std::thread th0; // CLI Thread
	std::thread th1;
	Terminal<Core>* term = nullptr;
	DisplayManager* dispMgr = nullptr;
	DeviceManager* devMgr = nullptr;
	
	bool LoadSettings();
	bool LoadTerminal();
	bool LoadDevices();
	bool LoadUI();
	void RefreshDevices();
	bool CanRun();
};

#endif