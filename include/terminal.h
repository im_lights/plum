#ifndef TERMINAL_H
#define TERMINAL_H

#include <sstream>
#include <iostream>
#include <string>
#include <vector>
#include <deque>
#include <map>
#include <locale>
#include <util.h>

enum ArgType { argBool, argNumber, argString };

struct ParsedArgument
{
	ParsedArgument(bool _default, std::string _key, ArgType _a, std::string _value) : def(_default), key(_key), type(_a), value(_value) {}
	bool def;
	std::string key;
	ArgType type;
	std::string value;
};

struct Argument
{
	Argument(const std::string _key, const ArgType _type);
	
	const std::string key;
	const ArgType type;
};

template<class Core>
struct Command
{
	Command(const std::string _key, const std::vector<Argument> _args, const std::vector<std::string> _req, void* lamda);
	
	const std::string key;
	const std::vector<Argument> args;
	const std::vector<std::string> requiredArgs;
	void* func;
	
	bool ParseArgument(std::string in, std::deque<ParsedArgument> &args_deq);
	bool run(std::deque<ParsedArgument> args);
};

template<class Core>
class Terminal
{
public:
	Terminal(std::map<std::string, Command<Core>> _cmds, Core* _m);
	~Terminal();
	
	static bool LoadCommands(bool full, std::map<std::string, Command<Core>>* cmds, Core* m);
	void AcceptInput();
	void PauseInput();
	bool ParseInput(std::string in);
	void Listen();
	void SetMode(bool m);
private:
	Core* Master;
	bool cmdMode;
	bool awaiting = false;
	bool allowInput = false;
	std::map<std::string, Command<Core>> commands;
	std::deque<std::string> lineHistory;
	
	void a();
	const std::vector<std::string> CommandList();
	bool TryCorrect(std::string input, std::string &output, const std::vector<std::string> commandList);
	bool ValidateCommand(std::string &cmd);
	bool VerifyArguments(std::string cmd, std::deque<std::string> &parts, std::deque<ParsedArgument> &args);
	bool ExecuteCommand(std::string cmd, std::deque<ParsedArgument> args);
};

#endif