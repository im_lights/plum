#ifndef FEED_H
#define FEED_H

#pragma once

#include <string>

struct FeedPipe
{
	FeedPipe(std::string _id, int h, int w);
	~FeedPipe();
	
	int height;
	int width;
	std::string id;
	bool newframe = false;
	
	void write(const void* ptr, int size);
};

#endif